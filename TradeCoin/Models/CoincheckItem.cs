﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TradeCoin.Models
{
    public class CoincheckItem
    {
        public double btc { get; set; }
        public double eth { get; set; }
        public double etc { get; set; }
        public double lsk { get; set; }
        public double fct { get; set; }
        public double xmr { get; set; }
        public double rep { get; set; }
        public double xrp { get; set; }
        public double zec { get; set; }
        public double xem { get; set; }
        public double ltc { get; set; }
        public double dash { get; set; }
        public double bch { get; set; }
    }
}