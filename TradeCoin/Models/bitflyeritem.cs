﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TradeCoin.Models
{
    public class bitflyeritem
    {
        public string currency_code { get; set; }
        public string base_currency { get; set; }
        public double rate_to_base_currency { get; set; }
    }
}