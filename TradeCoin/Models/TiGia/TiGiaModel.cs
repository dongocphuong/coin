﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TradeCoin.Models.TiGia
{
    public class TiGiaModel
    {
        public double YenNhat { get; set; }
        public double USD { get; set; }
        public double Won { get; set; }
        public double USDT { get; set; }
    }
}