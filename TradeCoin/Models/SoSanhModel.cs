﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TradeCoin.Models
{
    public class SoSanhModel
    {
        public string Coin { get; set; }
        public string SiteLon { get; set; }
        public string SiteBe { get; set; }
        public double TiLe { get; set; }
        public double TienLai { get; set; }
        public double GiaLon { get; set; }
        public double GiaBe { get; set; }
    }

    public class ListCoin
    {
        public string Coin { get; set; }
        public double TiLe { get; set; }
        public string SiteLon { get; set; }
        public string SiteBe { get; set; }
        public double TienLai { get; set; }
    }

    public class ItemsCoin
    {
        public string Site { get; set; }
        public string GiaTri { get; set; }
        public string Coin { get; set; }
    }

    public class DanhSachSanMode
    {
        public string TenSite { get; set; }
        public double Gia { get; set; }
        public string Coin { get; set; }
    }
}