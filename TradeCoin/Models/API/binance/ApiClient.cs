﻿using Binance.API.Csharp.Client.Domain.Abstract;
using Binance.API.Csharp.Client.Domain.Interfaces;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json.Linq;
using TradeCoin.Models.API;
using Binance.API.Csharp.Client.Utils;
using System.IO;

namespace Binance.API.Csharp.Client
{
    public class ApiClient : ApiClientAbstract, IApiClient
    {

        /// <summary>
        /// ctor.
        /// </summary>
        /// <param name="apiKey">Key used to authenticate within the API.</param>
        /// <param name="apiSecret">API secret used to signed API calls.</param>
        /// <param name="apiUrl">API base url.</param>
        public ApiClient(string apiKey, string apiSecret, string apiUrl = @"https://api.binance.com", string webSocketEndpoint = @"wss://stream.binance.com:9443/ws/", bool addDefaultHeaders = true)
            : base(apiKey, apiSecret, apiUrl, webSocketEndpoint, addDefaultHeaders)
        {
        }

        /// <summary>
        /// Calls API Methods.
        /// </summary>
        /// <typeparam name="T">Type to which the response content will be converted.</typeparam>
        /// <param name="method">HTTPMethod (POST-GET-PUT-DELETE)</param>
        /// <param name="endpoint">Url endpoing.</param>
        /// <param name="isSigned">Specifies if the request needs a signature.</param>
        /// <param name="parameters">Request parameters.</param>
        /// <returns></returns>
        public async Task<T> CallAsync<T>(ApiMethod method, string endpoint, bool isSigned = false, string parameters = null)
        {
            var finalEndpoint = endpoint + (string.IsNullOrWhiteSpace(parameters) ? "" : parameters);

            if (isSigned)
            {
                // Joining provided parameters
                //parameters += (!string.IsNullOrWhiteSpace(parameters) ? "&timestamp=" : "timestamp=") + Utilities.GenerateTimeStamp(DateTime.Now.ToUniversalTime());

                // Creating request signature
                var signature = Utilities.GenerateSignature(_apiSecret, parameters);
                finalEndpoint = endpoint + "?" + parameters + "&signature=" + signature;
            }

            var request = new HttpRequestMessage(Utilities.CreateHttpMethod(method.ToString()), finalEndpoint);
            //request.Headers.Add("X-MBX-APIKEY", this._apiKey);
            var response = await _httpClient.SendAsync(request).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                // Api return is OK
                response.EnsureSuccessStatusCode();

                // Get the result
                var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                // Serialize and return result
                return JsonConvert.DeserializeObject<T>(result);
            }

            // We received an error
            if (response.StatusCode == HttpStatusCode.GatewayTimeout)
            {
                throw new Exception("Api Request Timeout.");
            }

            // Get te error code and message
            var e = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            // Error Values
            var eCode = 0;
            string eMsg = "";
            if (e.IsValidJson())
            {
                try
                {
                    var i = JObject.Parse(e);

                    eCode = i["code"]!=null? (int)i["code"] : 0;
                    eMsg = i["msg"]!=null?(string)i["msg"] : "";
                } 
                catch { }
            }

            throw new Exception(string.Format("Api Error Code: {0} Message: {1}", eCode, eMsg));
        }

        public JObject xcoinApiCall(ApiMethod method, string endpoint, bool isSigned = false, string parameters = null)
        {
           
            HttpStatusCode nCode = 0;
            var finalEndpoint = endpoint + (string.IsNullOrWhiteSpace(parameters) ? "" : parameters);

            if (isSigned)
            {
                // Creating request signature
                var signature = Utilities.GenerateSignature(_apiSecret, parameters);
                finalEndpoint = endpoint + "?" + parameters + "&signature=" + signature;
            }
            try
            {
                HttpWebRequest Request = (HttpWebRequest)WebRequest.Create(this._apiUrl + finalEndpoint);
                Request.Headers.Add("X-MBX-APIKEY", this._apiKey);
                Request.Method =method.ToString();
                var Response = (HttpWebResponse)Request.GetResponse();
                var sRespBodyData = new StreamReader(Response.GetResponseStream()).ReadToEnd();

                return (JObject.Parse(sRespBodyData));
            }
            catch (WebException webEx)
            {
                using (HttpWebResponse Response = (HttpWebResponse)webEx.Response)
                {
                    nCode = Response.StatusCode;

                    using (StreamReader reader = new StreamReader(Response.GetResponseStream()))
                    {
                       var sRespBodyData = reader.ReadToEnd();

                        return (JObject.Parse(sRespBodyData));
                    }
                }
            }

            return (null);
        }
       
    }
}
