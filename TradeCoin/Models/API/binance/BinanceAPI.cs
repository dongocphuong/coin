﻿using Binance.API.Csharp.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TradeCoin.Models.API
{
    public class BinanceAPI
    {
        public string sAPI_Key = "qkuM9UoXflLrhl5dkoEbbE3PQqP9GKPcD6UMdAsjNSwfsPbg6wLYPKagOsZHX2Xx";
        public string sAPI_Secret = "rLOxqUtPMoqLcY1an6STIQ7pJzLypmic1LfV2osK6U69XrFa46ZG9ORbbc8K8S8R";
       

        public class ServerInfo
        {
            [JsonProperty("serverTIme")]
            public long ServerTime { get; set; }
        }
        public long GetTime()
        {
            var api = new ApiClient(sAPI_Key, sAPI_Secret);
            var result = api.CallAsync<ServerInfo>(ApiMethod.GET, "/api/v1/time", false);
            return result.Result.ServerTime;

        }
        public class depositAddress
        {
            public string address { get; set; }
            public string success { get; set; }
            public string addressTag { get; set; }
            public string asset { get; set; }
        }
        public JObject GetDepositAddress(string currency)
        {

            var api = new ApiClient(sAPI_Key, sAPI_Secret);
            string sParams = "asset=" + currency + "&recvWindow=" + 10000 + "&timestamp="+GetTime();
            var result = api.xcoinApiCall(ApiMethod.GET, "/wapi/v3/depositAddress.html", true, sParams);
            return result;
        }
        public JObject WithDraw(string currency, double amount, string address)
        {

            var api = new ApiClient(sAPI_Key, sAPI_Secret);
            string sParams = "asset=" + currency + "&recvWindow=" + 10000 + "&timestamp=" + GetTime() + "&amount=" + amount + "&address=" + address;
            var result = api.xcoinApiCall(ApiMethod.GET, "/wapi/v3/withdraw.html", true, sParams);
            return result;
        }
        public JObject BuyCoin(string currency, double quantity, double price)
        {

            var api = new ApiClient(sAPI_Key, sAPI_Secret);
            string sParams = "symbol=" + (currency.ToUpper() + "USDT") + "&recvWindow=" + 10000000 + "&timestamp=" + GetTime() + "&side=BUY" + "&type=LIMIT" + "&quantity=" + quantity + "&price=" + price + "&timeInForce=GTC";
            var result = api.xcoinApiCall(ApiMethod.POST, "/api/v3/order", true, sParams);
            return result;
        }
        public JObject SellCoin(string currency, double quantity, double price)
        {

            var api = new ApiClient(sAPI_Key, sAPI_Secret);
            string sParams = "symbol=" + (currency.ToUpper() + "USDT") + "&recvWindow=" + 10000000 + "&timestamp=" + GetTime() + "&side=SELL" + "&type=LIMIT" + "&quantity=" + quantity + "&price=" + price + "&timeInForce=GTC";
            var result = api.xcoinApiCall(ApiMethod.POST, "/api/v3/order", true, sParams);
            return result;
        }
    }
}