﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace TradeCoin.Models.API
{
    public class BithumbAPI
    {
        public string sAPI_Key = "392b894dad65ac9ec1d680056a7bf6b1";
        public string sAPI_Secret = "c38353abd55f00139333b9e00751bdac";
        public string typeMua = "purchase";
        public string typeBan = "sales";
        public string Account(string currency)
        {
           
            string url = "https://api.bithumb.com";
            string sParams = "currency=" + currency + "&apiKey=" + sAPI_Key + "&secretKey=" + sAPI_Secret;
            string sRespBodyData = String.Empty;
            XCoinAPI hAPI_Svr;
            JObject JObj = null;
            hAPI_Svr = new XCoinAPI(sAPI_Key, sAPI_Secret, url);
            JObj = hAPI_Svr.xcoinApiCall("/info/account", sParams, ref sRespBodyData);
            if (JObj == null)
            {
                return sRespBodyData;
            }
            else
            {
                return JObj.ToString();
            }

        }
        public string Balance(string currency)
        {

            string url = "https://api.bithumb.com";
            string sParams = "currency=" + currency + "&apiKey=" + sAPI_Key + "&secretKey=" + sAPI_Secret;
            string sRespBodyData = String.Empty;
            XCoinAPI hAPI_Svr;
            JObject JObj = null;
            hAPI_Svr = new XCoinAPI(sAPI_Key, sAPI_Secret, url);
            JObj = hAPI_Svr.xcoinApiCall("/info/balance", sParams, ref sRespBodyData);
            if (JObj == null)
            {
                return sRespBodyData;
            }
            else
            {
                return JObj.ToString();
            }

        }
        public string Order_detail(string order_id, string type, string currency)
        {

            string url = "https://api.bithumb.com";
            string sParams = "currency=" + currency + "&apiKey=" + sAPI_Key + "&secretKey=" + sAPI_Secret + "&type=" + type + "&order_id=" + order_id;
            string sRespBodyData = String.Empty;
            XCoinAPI hAPI_Svr;
            JObject JObj = null;
            hAPI_Svr = new XCoinAPI(sAPI_Key, sAPI_Secret, url);
            JObj = hAPI_Svr.xcoinApiCall("/info/order_detail", sParams, ref sRespBodyData);
            if (JObj == null)
            {
                return sRespBodyData;
            }
            else
            {
                return JObj.ToString();
            }

        }
        public string Coin_withdrawal(double units, string type, string currency, string destination,string address)
        {
            //address	    String	Currency withdrawing address (BTC, ETH, DASH, LTC, ETC, XRP, BCH, XMR, ZEC, QTUM, BTG, EOS)
            //destination	Integer	Currency withdrawal Destination Tag (when withdraw XRP)
            //              String	Currency withdrawal Payment Id (when withdraw XMR)
            //currency	    String	BTC, ETH, DASH, LTC, ETC, XRP, BCH, XMR, ZEC, QTUM, BTG, EOS (default value: BTC)
            string url = "https://api.bithumb.com";
            string sParams = "currency=" + currency + "&apiKey=" + sAPI_Key + "&secretKey=" + sAPI_Secret + "&type=" + type + "&units=" + units + "&address=" + address + "&destination=" + destination;
            string sRespBodyData = String.Empty;
            XCoinAPI hAPI_Svr;
            JObject JObj = null;
            hAPI_Svr = new XCoinAPI(sAPI_Key, sAPI_Secret, url);
            JObj = hAPI_Svr.xcoinApiCall("/trade/btc_withdrawal", sParams, ref sRespBodyData);
            if (JObj == null)
            {
                return sRespBodyData;
            }
            else
            {
                return JObj.ToString();
            }

        }
        public string Krw_withdrawal(int price, string bank, string account)
        {
            string url = "https://api.bithumb.com";
            string sParams = "apiKey=" + sAPI_Key + "&secretKey=" + sAPI_Secret + "&price=" + price + "&bank=" + bank + "&account=" + account;
            string sRespBodyData = String.Empty;
            XCoinAPI hAPI_Svr;
            JObject JObj = null;
            hAPI_Svr = new XCoinAPI(sAPI_Key, sAPI_Secret, url);
            JObj = hAPI_Svr.xcoinApiCall("/trade/krw_withdrawal", sParams, ref sRespBodyData);
            if (JObj == null)
            {
                return sRespBodyData;
            }
            else
            {
                return JObj.ToString();
            }

        }
        public string Market_buy(double units, string currency)
        {
            string url = "https://api.bithumb.com";
            string sParams = "apiKey=" + sAPI_Key + "&secretKey=" + sAPI_Secret + "&units=" + units + "&currency=" + currency;
            string sRespBodyData = String.Empty;
            XCoinAPI hAPI_Svr;
            JObject JObj = null;
            hAPI_Svr = new XCoinAPI(sAPI_Key, sAPI_Secret, url);
            JObj = hAPI_Svr.xcoinApiCall("/trade/market_buy", sParams, ref sRespBodyData);
            if (JObj == null)
            {
                return sRespBodyData;
            }
            else
            {
                return JObj.ToString();
            }

        }
        public string Market_sell(double units, string currency)
        {
            string url = "https://api.bithumb.com";
            string sParams = "apiKey=" + sAPI_Key + "&secretKey=" + sAPI_Secret + "&units=" + units + "&currency=" + currency;
            string sRespBodyData = String.Empty;
            XCoinAPI hAPI_Svr;
            JObject JObj = null;
            hAPI_Svr = new XCoinAPI(sAPI_Key, sAPI_Secret, url);
            JObj = hAPI_Svr.xcoinApiCall("/trade/market_sell", sParams, ref sRespBodyData);
            if (JObj == null)
            {
                return sRespBodyData;
            }
            else
            {
                return JObj.ToString();
            }

        }
    }


   
}