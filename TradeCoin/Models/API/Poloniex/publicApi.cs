﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace TradeCoin.Models.API.Poloniex
{
    /// <summary>
    /// https://poloniex.com/
    /// </summary>
    public class PPublicApi
    {
        private string __end_point;
        
        /// <summary>
        /// 
        /// </summary>
        public PPublicApi(string end_point = "public")
        {
            __end_point = end_point;
        }

        private PoloniexClient __public_client = null;

        private PoloniexClient PublicClient
        {
            get
            {
                if (__public_client == null)
                    __public_client = new PoloniexClient();
                return __public_client;
            }
        }

        /// <summary>
        /// poloniex 거래소 마지막 거래 정보
        /// </summary>
        /// <returns></returns>
        public  Task<Dictionary<string, PublicTicker>> GetTicker()
        {
            var _params = new Dictionary<string, object>();
            {
                _params.Add("command", "returnTicker");
            }

            return  PublicClient.CallApiGetAsync<Dictionary<string, PublicTicker>>(__end_point, _params);
        }


    }
}