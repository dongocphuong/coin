﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TradeCoin.Models.API
{
    public enum ApiMethod
    {
        POST,
        GET,
        PUT,
        DELETE
    }
}