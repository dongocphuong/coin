﻿using Binance.API.Csharp.Client.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace TradeCoin.Models.API.Coincheck
{
    public class CoinOneBaseApi
    {
        private string m_sAPI_URL = "";
        private string m_sAPI_Secret = "";
        private string m_Access_Token = "";

        public CoinOneBaseApi(string Access_Token, string sAPI_Secret, string sAPI_URL)
        {
            this.m_sAPI_Secret = sAPI_Secret;
            this.m_sAPI_URL = sAPI_URL;
            this.m_Access_Token = Access_Token;
        }


        private string ByteToString(byte[] rgbyBuff)
        {
            string sHexStr = "";


            for (int nCnt = 0; nCnt < rgbyBuff.Length; nCnt++)
            {
                sHexStr += rgbyBuff[nCnt].ToString("x2"); // Hex format
            }

            return (sHexStr);
        }


        private byte[] StringToByte(string sStr)
        {
            byte[] rgbyBuff = Encoding.UTF8.GetBytes(sStr);

            return (rgbyBuff);
        }


        private long MicroSecTime()
        {
            long nEpochTicks = 0;
            long nUnixTimeStamp = 0;
            long nNowTicks = 0;
            long nowMiliseconds = 0;
            string sNonce = "";
            DateTime DateTimeNow;


            nEpochTicks = new DateTime(1970, 1, 1).Ticks;
            DateTimeNow = DateTime.UtcNow;
            nNowTicks = DateTimeNow.Ticks;
            nowMiliseconds = DateTimeNow.Millisecond;

            nUnixTimeStamp = ((nNowTicks - nEpochTicks) / TimeSpan.TicksPerSecond);

            sNonce = nUnixTimeStamp.ToString() + nowMiliseconds.ToString("D03");

            return (Convert.ToInt64(sNonce));
        }


        private string Hash_HMAC(string sKey, string sData)
        {
            byte[] rgbyKey = Encoding.UTF8.GetBytes(sKey);


            using (var hmacsha512 = new HMACSHA512(rgbyKey))
            {
                hmacsha512.ComputeHash(Encoding.UTF8.GetBytes(sData));

                return (ByteToString(hmacsha512.Hash));
            }
        }


        public JObject xcoinApiCall(string sEndPoint, string sParams,string MeThod)
        {
            try
            {
                HttpWebRequest Request = (HttpWebRequest)WebRequest.Create(this.m_sAPI_URL + sEndPoint + "?" + sParams);
                var _payload = Convert.ToBase64String(Encoding.UTF8.GetBytes(sParams));
                var _signature = "";

                var _secretKey = Encoding.UTF8.GetBytes(this.m_sAPI_Secret.ToUpper());
                using (var _hmac = new HMACSHA512(_secretKey))
                {
                    _hmac.Initialize();

                    var _bytes = Encoding.UTF8.GetBytes(_payload);
                    var _rawHmac = _hmac.ComputeHash(_bytes);
                    _signature = Utilities.BytesToHex(_rawHmac);
                }
                Request.Headers.Add("X-COINONE-PAYLOAD", _payload);
                Request.Headers.Add("X-COINONE-SIGNATURE", _signature);
                Request.Method = MeThod;
                var Response = (HttpWebResponse)Request.GetResponse();
                var sRespBodyData = new StreamReader(Response.GetResponseStream()).ReadToEnd();

                return (JObject.Parse(sRespBodyData));
            }
            catch (WebException webEx)
            {
                using (HttpWebResponse Response = (HttpWebResponse)webEx.Response)
                {

                    using (StreamReader reader = new StreamReader(Response.GetResponseStream()))
                    {
                        var sRespBodyData = reader.ReadToEnd();

                        return (JObject.Parse(sRespBodyData));
                    }
                }
            }
        }
    }
}