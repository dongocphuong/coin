﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TradeCoin.Models.TiGia;

namespace TradeCoin.Models
{
    public class SiteModel
    {
        public string SiteName { get; set; }
        public List<binance> Coins { get; set; }
        public double Price { get; set; }
        public static List<SiteModel> GetAllSite()
        {
            var lst = new List<SiteModel>();
            var tigia = Function.LayTiGia();
            //1
            var smd = new SiteModel()
            {
                SiteName = "coincheck",
                Price = tigia.YenNhat,
                //Coins = Function.Getcoincheck()

            };
            lst.Add(smd);
            //2
            smd = new SiteModel()
            {
                SiteName = "bitflyer",
                Price = tigia.YenNhat,
                //Coins = Function.GetBitFly()

            };
            lst.Add(smd);
            //3
            smd = new SiteModel()
            {
                SiteName = "binance",
                Price = tigia.USDT,
                //Coins = Function.GetBinance()

            };
            lst.Add(smd);
            //4
            smd = new SiteModel()
            {
                SiteName = "coinbase",
                Price = tigia.USD,
                //Coins = Function.GetGdax()

            };
            lst.Add(smd);
            //5
            smd = new SiteModel()
            {
                SiteName = "bithumb",
                Price = tigia.Won,
                //Coins = Function.Getbithumb()

            };
            lst.Add(smd);
            ////6
            smd = new SiteModel()
            {
                SiteName = "coinone",
                Price = tigia.Won,
                //Coins = Function.GetCoinone()

            };
            lst.Add(smd);
            //
            smd = new SiteModel()
            {
                SiteName = "huobipro",
                Price = tigia.USDT,
                //Coins = Function.GetHuobipro()
            };
            lst.Add(smd);
            //
            smd = new SiteModel()
            {
                SiteName = "bitfinex",
                Price = tigia.USD,
                //Coins = Function.GetBitfinex()
            };
            lst.Add(smd);

            smd = new SiteModel()
            {
                SiteName = "bitbank",
                Price = tigia.YenNhat,
                //Coins = Function.GetBitfinex()
            };
            lst.Add(smd);
            return lst;
        }
        public static List<SiteModel> GetComPareSite(string Site1,string Site2)
        {
            var lst = new List<SiteModel>();
            //
            var tigia = Function.LayTiGia();
            if (Site1 == "coincheck" || Site2 == "coincheck")
            {
                var smd = new SiteModel()
                {
                    SiteName = "coincheck",
                    Price = tigia.YenNhat,
                    //Coins = Function.Getcoincheck()

                };

                lst.Add(smd);
            }
            //
            if (Site1 == "bitflyer" || Site2 == "bitflyer")
            {
                var smd = new SiteModel()
                {
                    SiteName = "bitflyer",
                    Price = tigia.YenNhat,
                    //Coins = Function.GetBitFly()

                };

                lst.Add(smd);
            }
            //
            if (Site1 == "binance" || Site2 == "binance")
            {
                var smd = new SiteModel()
                {
                    SiteName = "binance",
                    Price = tigia.USDT,
                    //Coins = Function.GetBinance()

                };
                lst.Add(smd);
            }
            //
            if (Site1 == "coinbase" || Site2 == "coinbase")
            {
                var smd = new SiteModel()
                {
                    SiteName = "coinbase",
                    Price = tigia.USD,
                    //Coins = Function.GetGdax()

                };
                lst.Add(smd);
            }
            //
            if (Site1 == "bithumb" || Site2 == "bithumb")
            {
                var smd = new SiteModel()
                {
                    SiteName = "bithumb",
                    Price = tigia.Won,
                    //Coins = Function.Getbithumb()

                };
                lst.Add(smd);
            }
            
            if (Site1 == "coinone" || Site2 == "coinone")
            {
                var smd = new SiteModel()
                {
                    SiteName = "coinone",
                    Price = tigia.Won,
                    //Coins = Function.GetCoinone()

                };
                lst.Add(smd);
            }
            if (Site1 == "huobipro" || Site2 == "huobipro")
            {
                var smd = new SiteModel()
                {
                    SiteName = "huobipro",
                    Price = tigia.USDT,
                    //Coins = Function.GetHuobipro()
                };
                lst.Add(smd);
            }
            if (Site1 == "bitfinex" || Site2 == "bitfinex")
            {
               var smd = new SiteModel()
                {
                    SiteName = "bitfinex",
                    Price = tigia.USD,
                    //Coins = Function.GetBitfinex()
                };
               lst.Add(smd);
            }
            
            //
            if (Site1 == "bitbank" || Site2 == "bitbank")
            {
                var smd = new SiteModel()
                {
                    SiteName = "bitbank",
                    Price = tigia.YenNhat,
                    //Coins = Function.GetBitfinex()
                };
                lst.Add(smd);
            }

            return lst;
        }
    }
    public class Function
    {
        public static List<binance> Getcoincheck()
        {
            var lst = new List<binance>();
            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    var json = webClient.DownloadString("https://coincheck.com/api/rate/all");
                    var a = JsonConvert.DeserializeObject<Coincheck>(json);

                    lst.Add(new binance() { symbol = "btc", lastPrice = a.jpy.btc });
                    lst.Add(new binance() { symbol = "eth", lastPrice = a.jpy.eth });
                    lst.Add(new binance() { symbol = "etc", lastPrice = a.jpy.etc });
                    lst.Add(new binance() { symbol = "lsk", lastPrice = a.jpy.lsk });
                    lst.Add(new binance() { symbol = "fct", lastPrice = a.jpy.fct });
                    lst.Add(new binance() { symbol = "xmr", lastPrice = a.jpy.xmr });
                    lst.Add(new binance() { symbol = "rep", lastPrice = a.jpy.rep });
                    lst.Add(new binance() { symbol = "xrp", lastPrice = a.jpy.xrp });
                    lst.Add(new binance() { symbol = "zec", lastPrice = a.jpy.zec });
                    lst.Add(new binance() { symbol = "xem", lastPrice = a.jpy.xem });
                    lst.Add(new binance() { symbol = "ltc", lastPrice = a.jpy.ltc });
                    lst.Add(new binance() { symbol = "dash", lastPrice = a.jpy.dash });
                    lst.Add(new binance() { symbol = "bch", lastPrice = a.jpy.bch });

                }
            }
            catch (Exception ex)
            {

            }
            return lst;
        }
        public static List<binance> GetBitFly()
        {
            List<binance> lst = new List<binance>();
            try
            {
                string URL = "https://bitflyer.jp/ex.asmx/GetFxRate";
                string DATA = @"{ base_currency: 'JPY', lang: 'en-US' }";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = DATA.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Write(DATA);
                requestWriter.Close();
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                var a = JsonConvert.DeserializeObject<bitflyer>(response);
                lst = a.d.Select(p => new binance { symbol = p.currency_code, lastPrice = p.rate_to_base_currency }).ToList();
            }
            catch (Exception ex)
            {

            }
            return lst;
        }
        public static List<binance> GetBinance()
        {
            List<binance> a = new List<binance>();
            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    var json = webClient.DownloadString("https://api.binance.com/api/v1/ticker/24hr");
                    a = JsonConvert.DeserializeObject<List<binance>>(json);
                    a = a.Where(p => p.symbol.ToLower().Contains("usdt")).Select(p => new binance { symbol = p.symbol.ToLower().Replace("usdt", ""), lastPrice = p.lastPrice }).ToList();

                }
            }
            catch (Exception ex)
            {

            }
            return a;
        }
        public static List<binance> GetGdax()
        {

            List<binance> lst = new List<binance>();
            try
            {
                string URL = "https://api.gdax.com/products";
                string DATA = @"{ }";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                var a = JsonConvert.DeserializeObject<List<gdax>>(response);
                a = a.Where(p => p.quote_currency.ToLower().Contains("usd")).ToList();
                foreach (var item in a)
                {
                    using (var webClient = new System.Net.WebClient())
                    {
                        URL = "https://api.gdax.com/products/" + item.id + "/stats";
                        DATA = @"{ }";
                        request = (HttpWebRequest)WebRequest.Create(URL);
                        request.Method = "GET";
                        request.ContentType = "application/json";
                        request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
                        webResponse = request.GetResponse();
                        webStream = webResponse.GetResponseStream();
                        responseReader = new StreamReader(webStream);
                        response = responseReader.ReadToEnd();
                        var a2 = JsonConvert.DeserializeObject<gdaxitem>(response);
                        //var json2 = webClient.DownloadString("");
                        //var a2 = JsonConvert.DeserializeObject<gdaxitem>(json2);
                        var bn = new binance()
                        {
                            lastPrice = a2.last,
                            symbol = item.base_currency
                        };
                        lst.Add(bn);

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return lst;

        }
        public static List<binance> Getbithumb()
        {
            using (var webClient = new System.Net.WebClient())
            {
                var json = webClient.DownloadString("https://api.bithumb.com/public/ticker/All");
                var a = JsonConvert.DeserializeObject<bithumb>(json);
                var lst = new List<binance>();
                lst.Add(new binance() { symbol = "btc", lastPrice = a.data.BTC.sell_price });
                lst.Add(new binance() { symbol = "eth", lastPrice = a.data.ETH.sell_price });
                lst.Add(new binance() { symbol = "dash", lastPrice = a.data.DASH.sell_price });
                lst.Add(new binance() { symbol = "LTC".ToLower(), lastPrice = a.data.LTC.sell_price });
                lst.Add(new binance() { symbol = "ETC".ToLower(), lastPrice = a.data.ETC.sell_price });
                lst.Add(new binance() { symbol = "XRP".ToLower(), lastPrice = a.data.XRP.sell_price });
                lst.Add(new binance() { symbol = "BCH".ToLower(), lastPrice = a.data.BCH.sell_price });
                lst.Add(new binance() { symbol = "XMR".ToLower(), lastPrice = a.data.XMR.sell_price });
                lst.Add(new binance() { symbol = "ZEC".ToLower(), lastPrice = a.data.ZEC.sell_price });
                lst.Add(new binance() { symbol = "QTUM".ToLower(), lastPrice = a.data.QTUM.sell_price });
                lst.Add(new binance() { symbol = "BTG".ToLower(), lastPrice = a.data.BTG.sell_price });
                lst.Add(new binance() { symbol = "EOS".ToLower(), lastPrice = a.data.EOS.sell_price });
                return lst;
            }
        }
        public static List<binance> GetCoinone()
        {
            using (var webClient = new System.Net.WebClient())
            {
                var json = webClient.DownloadString("https://api.coinone.co.kr/ticker?currency=all&format=json");
                var a = JsonConvert.DeserializeObject<coinone>(json);
                var lst = new List<binance>();
                lst.Add(new binance() { symbol = "bch", lastPrice = a.bch.last });
                lst.Add(new binance() { symbol = "qtum", lastPrice = a.qtum.last });
                lst.Add(new binance() { symbol = "iota", lastPrice = a.iota.last });
                lst.Add(new binance() { symbol = "etc", lastPrice = a.etc.last });
                lst.Add(new binance() { symbol = "ltc", lastPrice = a.ltc.last });
                lst.Add(new binance() { symbol = "btc", lastPrice = a.btc.last });
                lst.Add(new binance() { symbol = "btg", lastPrice = a.btg.last });
                lst.Add(new binance() { symbol = "eth", lastPrice = a.eth.last });
                lst.Add(new binance() { symbol = "xrp", lastPrice = a.xrp.last });
                return lst;
            }
        }
        public static List<binance> GetHuobipro()
        {
            using (var webClient = new System.Net.WebClient())
            {
                var lst = new List<binance>();
                try
                {
                    ServicePointManager.SecurityProtocol =SecurityProtocolType.Tls12;
                    ServicePointManager.Expect100Continue = true;
                    var json = webClient.DownloadString("https://api.huobi.pro/v1/common/symbols");
                    var a = JsonConvert.DeserializeObject<dynamic>(json);

                    var lg = (IEnumerable<dynamic>)a["data"];
                    foreach (var item in lg)
                    {
                        if (item["quote-currency"] == "usdt" && item["symbol-partition"] == "main")
                        {
                            var json2 = webClient.DownloadString("https://api.huobi.pro/market/history/kline?symbol=" + (item["base-currency"] + item["quote-currency"]));
                            var b = JsonConvert.DeserializeObject<dynamic>(json2);
                            lst.Add(new binance() { symbol = item["base-currency"], lastPrice = b["data"][0]["low"] });
                        }
                    }
                }
                catch (Exception ex)
                {
                    
                }
                return lst;
            }
            //var lst = new List<binance>();
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //string URL = "https://api.huobi.pro/v1/common/symbols";
            //string DATA = @"{ }";
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            //request.Method = "GET";
            //request.ContentType = "application/json";
            //request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
            //request.UseDefaultCredentials = true;
            //request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
            //WebResponse webResponse = request.GetResponse();
            //Stream webStream = webResponse.GetResponseStream();
            //StreamReader responseReader = new StreamReader(webStream);
            //string response = responseReader.ReadToEnd();


            //var a = JsonConvert.DeserializeObject<dynamic>(response);
            //for (int i = 0; i < a.data.length; i++)
            //{
            //    URL = "https://api.huobi.pro/market/history/kline?symbol=" + (a.data[i][0] + a.data[i][1]);
            //    DATA = @"{ }";
            //    request = (HttpWebRequest)WebRequest.Create(URL);
            //    request.Method = "GET";
            //    request.ContentType = "application/json";
            //    request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
            //    webResponse = request.GetResponse();
            //    webStream = webResponse.GetResponseStream();
            //    responseReader = new StreamReader(webStream);
            //    response = responseReader.ReadToEnd();
            //    var b = JsonConvert.DeserializeObject<dynamic>(response);
            //    lst.Add(new binance() { symbol = a.data[i][0], lastPrice = b.data[0].low });
            //}
            //return lst;
        }
        public static List<binance> GetBitfinex()
        {
            using (var webClient = new System.Net.WebClient())
            {
                var lst = new List<binance>();
                try
                {
                    var json = webClient.DownloadString("https://api.bitfinex.com/v1/symbols");
                    var a = JsonConvert.DeserializeObject<List<string>>(json).Where(p => p.ToLower().Contains("usd"));
                    foreach (var item in a)
                    {
                        var json2 = webClient.DownloadString("https://api.bitfinex.com/v1/pubticker/" + item);
                        var b = JsonConvert.DeserializeObject<dynamic>(json2);
                        lst.Add(new binance() { symbol = item.ToLower().Replace("usd", ""), lastPrice = b["last_price"] });
                    }
                }
                catch (Exception ex)
                {

                }
                return lst;
            }
          
        }
        public static List<binance> GetPoloniex()
        {
            return new List<binance>();
        }
        public static List<binance> GetBitbank()
        {
            var lstbit = new List<string>() { "btc_jpy", "xrp_jpy","mona_jpy", "bcc_jpy" };
            using (var webClient = new System.Net.WebClient())
            {
                var lst = new List<binance>();
               
                try
                {
                    foreach (var item in lstbit)
                    {
                        var json = webClient.DownloadString("https://public.bitbank.cc/" + item + "/ticker");
                        var a = JsonConvert.DeserializeObject<dynamic>(json);
                        var b = new binance()
                        {
                            symbol = item.Split('_').FirstOrDefault(),
                            lastPrice = a["data"]["last"]
                        };
                        lst.Add(b);
                    }
                  
                   
                }
                catch (Exception ex)
                {

                }
                return lst;
            }

        }
        #region Ti Gia
        public static TiGiaModel LayTiGia()
        {
            var TiGia = new TiGiaModel();
            try
            {
                // From Web
                var url = "http://www.vietcombank.com.vn/ExchangeRates/";
                var web = new HtmlWeb();
                var doc = web.Load(url);
                // With LINQ	
                var value = doc.DocumentNode
                 .SelectNodes("//table[@id='Content_ExrateView']/tr/td");
                foreach (var tr in value)
                {
                    if (tr.InnerText.ToLower() == "krw")
                    {
                        TiGia.Won=Convert.ToDouble(tr.NextSibling.NextSibling.NextSibling.NextSibling.InnerText.Replace(",", ""));
                    }
                    if (tr.InnerText.ToLower() == "jpy")
                    {
                        TiGia.YenNhat = Convert.ToDouble(tr.NextSibling.NextSibling.NextSibling.NextSibling.InnerText.Replace(",", ""));
                    }
                    if (tr.InnerText.ToLower() == "usd")
                    {
                        TiGia.USD = Convert.ToDouble(tr.NextSibling.NextSibling.NextSibling.NextSibling.InnerText.Replace(",", ""));
                    }
                    
                }
                TiGia.USDT = LayTiGiaUSDT();
            }
            catch (Exception)
            {

            }
            return TiGia;
        }
        public static double LayTiGiaUSDT()
        {
            try
            {
                // From Web
                var url = "https://tiktakbtc.com/coin/BTC/VCB";
                var web = new HtmlWeb();
                var doc = web.Load(url);
                // With LINQ	
                var value = doc.DocumentNode
                 .SelectNodes("//div[@class='boxContent']/div[@class='buy']/a").LastOrDefault();
                return Convert.ToDouble(value.InnerText.Replace(",", ""));
            }
            catch (Exception ex)
            {

            }
            return 0;
        }
       
        #endregion
    }
}