﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TradeCoin.Models
{
    public class CoinModel
    {
    }

    public class gdax
    {
        public string id { get; set; }
        public string quote_currency { get; set; }
        public string base_currency { get; set; }

    }
    public class gdaxitem
    {
        public double last { get; set; }
    }
    public class bithumb
    {
        public bithumbitem data { get; set; }
    }
    public class bithumbitem
    {
        public bithumbitem2 BTC { get; set; }
        public bithumbitem2 ETH { get; set; }
        public bithumbitem2 DASH { get; set; }
        public bithumbitem2 LTC { get; set; }
        public bithumbitem2 ETC { get; set; }
        public bithumbitem2 XRP { get; set; }
        public bithumbitem2 BCH { get; set; }
        public bithumbitem2 XMR { get; set; }
        public bithumbitem2 ZEC { get; set; }
        public bithumbitem2 QTUM { get; set; }
        public bithumbitem2 BTG { get; set; }
        public bithumbitem2 EOS { get; set; }

    }
    public class bithumbitem2
    {
        public double sell_price { get; set; }
    }


    public class coinone
    {
        public coinoneitem bch { get; set; }
        public coinoneitem qtum { get; set; }
        public coinoneitem iota { get; set; }
        public coinoneitem etc { get; set; }
        public coinoneitem ltc { get; set; }
        public coinoneitem btc { get; set; }
        public coinoneitem btg { get; set; }
        public coinoneitem eth { get; set; }
        public coinoneitem xrp { get; set; }

    }
    public class coinoneitem
    {
        public double last { get; set; }
    }

    
}