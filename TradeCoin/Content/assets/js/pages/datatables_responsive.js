﻿/* ------------------------------------------------------------------------------
*
*  # Responsive extension for Datatables
*
*  Specific JS code additions for datatable_responsive.html page
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {


    // Table setup
    // ------------------------------

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: true,
        responsive: false,
        pageLength: 25,
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span> Tìm Kiếm :</span> _INPUT_',
            lengthMenu: '<span> Hiển Thị :</span> _MENU_',
            zeroRecords: "Không có kết quả",
            info: "_START_ - _END_ của _TOTAL_ kết quả ",
            infoEmpty: "Không có kết quả",
            paginate: { 'first': "Về đầu", 'last': "Về cuối", 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });



    // Whole row as a control
    



    // External table additions
    // ------------------------------

    // Add placeholder to the datatable filter option
    
    
});
