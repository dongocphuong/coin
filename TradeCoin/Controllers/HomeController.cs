﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TradeCoin.Models;
using System.Data;
using System.Reflection;
using HtmlAgilityPack;
using TradeCoin.Models.API;
using Newtonsoft.Json.Linq;


namespace TradeCoin.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<SoSanhModel> ss = new List<SoSanhModel>();
            ss = GetSoSanh();
            return View(ss);
        }

        public ActionResult GetTableCoin(string Site1 = "", string Site2 = "")
        {
            List<SoSanhModel> ss = new List<SoSanhModel>();
            ss = GetSoSanh(Site1,Site2);
            return PartialView("~/Views/Home/_TableCoin.cshtml", ss);
        }

        public List<SoSanhModel> GetSoSanh(string Site1="",string Site2="")
        {
            List<binance> lstCoinBySan = new List<binance>();

            var allsite = SiteModel.GetAllSite();
            if (Site1 != "" && Site2 != "")
            {
                allsite = SiteModel.GetComPareSite(Site1, Site2);
            }
            var ss = new List<SoSanhModel>();
            foreach (var site in allsite)
            {
                if (site.SiteName == "coincheck"){ site.Coins = Function.Getcoincheck();}
                if (site.SiteName == "bitflyer") { site.Coins = Function.GetBitFly(); };
                if (site.SiteName == "binance") { site.Coins = Function.GetBinance(); }
                if (site.SiteName == "coinbase") { site.Coins = Function.GetGdax(); }
                if (site.SiteName == "bithumb") { site.Coins = Function.Getbithumb(); }
                if (site.SiteName == "coinone") { site.Coins = Function.GetCoinone(); }
                if (site.SiteName == "huobipro") { site.Coins = Function.GetHuobipro(); }
                if (site.SiteName == "bitfinex") { site.Coins = Function.GetBitfinex(); }
                if (site.SiteName == "bitbank") { site.Coins = Function.GetBitbank(); }
                if (site.Coins.Any())
                {
                    lstCoinBySan.AddRange(site.Coins.Select(p => new binance
                    {
                        lastPrice= p.lastPrice*site.Price,
                        namesite= site.SiteName,
                        symbol=p.symbol
                    }));
                }
            }

            List<binance> coinChung = new List<binance>();
            foreach (var item in lstCoinBySan)
            {
                if (coinChung.Where(p => p.symbol.ToLower() == item.symbol.ToLower()).ToList().Count > 0)
                {
                    continue;
                }

                if (lstCoinBySan.Where(p => p.symbol.ToUpper() == item.symbol.ToUpper()).ToList().Count >= 2)
                {
                    List<binance> coinok = lstCoinBySan.Where(p => p.symbol.ToUpper() == item.symbol.ToUpper()).ToList();
                    coinChung.AddRange(coinok);
                }

            }

            List<binance> coinsosanh = new List<binance>();
            foreach (var item in coinChung)
            {
                if (ss.Where(p => p.Coin.ToLower() == item.symbol.ToLower()).ToList().Count > 0)
                {
                    continue;
                }
                List<binance> coinitem = new List<binance>();
                coinitem = coinChung.Where(p => p.symbol.ToLower() == item.symbol.ToLower() && p.namesite != item.namesite).ToList();
                foreach (var ic in coinitem)
                {
                    if (ic.lastPrice > item.lastPrice && ic.lastPrice>0 && item.lastPrice>0)
                    {
                        var sosanh = new SoSanhModel()
                        {
                            GiaBe = item.lastPrice,
                            GiaLon = ic.lastPrice,
                            Coin = item.symbol,
                            SiteBe = item.namesite,
                            SiteLon = ic.namesite,
                            TienLai = ic.lastPrice - item.lastPrice,
                            TiLe = (ic.lastPrice - item.lastPrice) / ic.lastPrice *100
                        };
                        ss.Add(sosanh);
                    }
                }
            }

            return ss.OrderBy(p => p.Coin).OrderByDescending(p => p.TiLe).ToList();
        }

        public ActionResult LayDanhSachSanByCoin(string symbol)
        {
            var ss = new List<DanhSachSanMode>();
            var allsite = SiteModel.GetAllSite();
            foreach (var site in allsite)
            {
                if (site.SiteName == "coincheck") { site.Coins = Function.Getcoincheck(); }
                if (site.SiteName == "bitflyer") { site.Coins = Function.GetBitFly(); };
                if (site.SiteName == "binance") { site.Coins = Function.GetBinance(); }
                if (site.SiteName == "coinbase") { site.Coins = Function.GetGdax(); }
                if (site.SiteName == "bithumb") { site.Coins = Function.Getbithumb(); }
                if (site.SiteName == "coinone") { site.Coins = Function.GetCoinone(); }
                if (site.SiteName == "huobipro") { site.Coins = Function.GetHuobipro(); }
                if (site.SiteName == "bitfinex") { site.Coins = Function.GetBitfinex(); }
                if (site.SiteName == "bitbank") { site.Coins = Function.GetBitbank(); }
                var coin = site.Coins.FirstOrDefault(p=>p.symbol.ToLower()==symbol.ToLower());
                if (coin!=null)
                {
                    ss.Add(new DanhSachSanMode()
                    {
                        Coin = site.Coins.FirstOrDefault().symbol.ToUpper(),
                        Gia = coin.lastPrice*site.Price,
                        TenSite = site.SiteName.ToUpper()
                    });
                }
            }
            return PartialView("~/Views/Home/_TableCoin_San.cshtml", ss);
        }

        public ActionResult TestApi(string symbol)
        {
           var a=Function.GetPoloniex();
            return PartialView();
        }

       
    }
}