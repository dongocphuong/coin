﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TradeCoin.Startup))]
namespace TradeCoin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
